# Yến Vua

## USER API

- Login: /api/v1/login
- getListProduct: /api/v1/product (Có thể sort giống như getListProduct của admin)
- getProductDetail: /api/v1/product/detail
- getListCategory: /api/v1/category
- createOrder: /api/v1/order

## ADMIN API

- Cần gửi kèm thêm token trong req.headers.authorization

### Upload image

- uploadImage: /api/admin/v1/upload

### Product

- getListProduct: GET /api/admin/v1/product
  -> Lấy danh sách theo category: /api/admin/v1/product?categoryId="id của category"
  -> Tìm theo từ khóa: /api/admin/v1/product?keyword="từ khóa"
  -> Sort: (desc là sort theo từ cao tới thấp, asc là sort từ thấp tới cao)
  - sort theo tên: /api/admin/v1/product?sortBy=(name:asc) or /api/admin/v1/product?sortBy=(name:desc)
  - sort theo giá: /api/admin/v1/product?sortBy=(price:asc) or /api/admin/v1/product?sortBy=(price:desc)
  - sort theo ngày cập nhật: /api/admin/v1/product?sortBy=(updatedAt:asc) or /api/admin/v1/product?sortBy=(updatedAt:desc)
- getProductDetail: GET /api/admin/v1/product/detail
- createProduct: POST /api/admin/v1/product
- updateProduct: PUT /api/admin/v1/product
- updateProductStatus: PUT /api/admin/v1/product/status
- deleteProduct: DELETE /api/admin/v1/product

### Category

- getListCategory: GET /api/admin/v1/category
- createCategory: POST /api/admin/v1/category
- updateCategory: PUT /api/admin/v1/category
- deleteCategory: DELETE /api/admin/v1/category (Tạm thời ko làm chức năng xóa category nha m)

### Order

- getListOrder: GET /api/admin/v1/order
  -> Sort: (desc là sort theo từ cao tới thấp, asc là sort từ thấp tới cao)

  - sort theo tổng tiền: /api/admin/v1/order?sortBy(total:asc) or /api/admin/v1/order?sortBy(total:desc)
  - sort theo ngày đặt hàng: /api/admin/v1/order?sortBy=(createdAt:asc) or /api/admin/v1/order?sortBy=(createdAt:desc)

  -> Filter theo status: /api/admin/v1/order?status=(['CREATED', 'DONE', 'CANCELED']) => Chọn 1 trong 3
  Một đơn hàng sẽ có 3 trang thái:

  - CREATED: Là đơn hàng mới
  - DONE: Là đơn hàng đã thành công
  - CANCELED: Là đơn hàng đã hủy

- getOrderDetail: GET /api/admin/v1/order/detail
- updateOrderStatus: POST /api/admin/v1/order/status -> gửi kèm body như sau

  ```shell
  {
    "orderId": "622dae60dd9c151935029cdc",
    "status": "DONE"
  }
  ```
