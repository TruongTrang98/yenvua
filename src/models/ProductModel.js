const mongoose = require('mongoose');

const productSchema = new mongoose.Schema(
  {
    code: {
      type: String,
      required: true,
      trim: true,
    },
    slug: {
      type: String,
      required: true,
      trim: true,
    },
    name: {
      type: String,
      required: true,
      trim: true,
    },
    image: { type: String },
    description: { type: String },
    categoryId: { type: mongoose.Schema.Types.ObjectId, ref: 'category' },
    shortDescription: { type: String },
    price: { type: Number, min: 0 },
    saleNumber: { type: Number, default: 0 },
    isActive: { type: Boolean, default: true },
    isDeleted: { type: Boolean, default: false },
  },
  {
    collection: 'product',
    timestamps: true,
    autoIndex: true,
    versionKey: false,
  }
);

module.exports = mongoose.model('product', productSchema);
