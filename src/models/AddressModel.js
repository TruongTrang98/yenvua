const mongoose = require('mongoose');

const addressSchema = new mongoose.Schema(
  {
    code: { type: Number, unique: true, required: true },
    name: { type: String, required: true, trim: true },
    parentCode: { type: Number },
    codename: { type: String, trim: true },
    short_codename: { type: String, trim: true },
    division_type: { type: String, trim: true },
  },
  {
    collection: 'address',
    timestamps: true,
    autoIndex: true,
    versionKey: false,
  }
);

module.exports = mongoose.model('address', addressSchema);
