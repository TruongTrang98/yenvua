const mongoose = require('mongoose');

const categorySchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    image: { type: String, required: true },
    isActive: {
      type: Boolean,
      default: true,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  {
    collection: 'category',
    versionKey: false,
    timestamps: true,
    autoIndex: true,
  }
);

module.exports = mongoose.model('category', categorySchema);
