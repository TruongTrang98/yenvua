const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema(
  {
    customerInfo: {
      name: { type: String, required: true, trim: true },
      gender: {
        type: String,
        required: true,
        enum: ['MALE', 'FEMALE', 'OTHER'],
      },
      phone: { type: String, required: true, trim: true },
      email: { type: String, required: true, trim: true },
      address: { type: Object, required: true },
    },
    // userId: {
    //   type: mongoose.Schema.Types.ObjectId,
    //   ref: 'userAccount',
    //   required: true,
    // },
    orderInfo: [
      {
        _id: false,
        productId: {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'product',
          required: true,
        },
        quantity: { type: Number, min: 1, required: true },
        price: { type: Number, min: 1, required: true },
        total: { type: Number, min: 1, required: true },
      },
    ],
    status: {
      type: String,
      enum: ['CREATED', 'DONE', 'CANCELED'],
      default: 'CREATED',
    },
    note: {
      type: String,
    },
    unit: {
      type: String,
      enum: ['VND'],
      default: 'VND',
    },
    total: {
      type: Number,
      required: true,
    },
  },
  {
    collection: 'order',
    timestamps: true,
    versionKey: false,
    autoIndex: true,
  }
);

module.exports = mongoose.model('order', orderSchema);
