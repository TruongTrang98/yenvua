const mongoose = require('mongoose');

const userAccountSchema = new mongoose.Schema(
  {
    phoneNumber: { type: String, unique: true, required: true },
    email: {
      type: String,
      trim: true,
    },
    password: { type: String },
    name: { type: String },
    address: { type: Object },
    gender: { type: String, enum: ['MALE', 'FEMALE', 'OTHER'] },
    status: {
      type: String,
      enum: ['active', 'inactive', 'pending'],
      default: 'pending',
    },
    registerTimes: { type: Number, default: 0 }, // Fail max 3 times
  },
  {
    collection: 'userAccount',
    timestamps: true,
    autoIndex: true,
    versionKey: false,
  }
);

module.exports = mongoose.model('userAccount', userAccountSchema);
