const mongoose = require('mongoose')

const accountSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      unique: true,
      required: true,
      lowercase: true,
      trim: true
    },
    password: { type: String, required: true },
    role: {
      type: String,
      enum: ['admin', 'user']
    },
    status: {
      type: String,
      enum: ['active', 'inactive']
    }
  },
  {
    collection: 'account',
    timestamps: true,
    autoIndex: true,
    versionKey: false
  }
)

module.exports = mongoose.model('account', accountSchema)
