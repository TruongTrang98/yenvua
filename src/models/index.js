const AccountModel = require('./AccountModel');
const AddressModel = require('./AddressModel');
const ProductModel = require('./ProductModel');
const OrderModel = require('./OrderModel');
const CategoryModel = require('./CategoryModel');
const UserAccountModel = require('./UserAccountModel');

module.exports = {
  AccountModel,
  AddressModel,
  ProductModel,
  OrderModel,
  CategoryModel,
  UserAccountModel,
};
