const jwt = require('jsonwebtoken');
const { configs, constants } = require('../configs/index');

module.exports = {
  sign: async (accountType, payload) => {
    return new Promise((resolve, reject) => {
      jwt.sign(
        payload,
        configs.token.secretKey,
        {
          algorithm: 'HS256',
          expiresIn:
            configs.token[constants.ACCOUNT_TYPE[accountType]].expiresIn,
        },
        (err, token) => {
          if (err) return reject(err);
          return resolve(token);
        }
      );
    });
  },
  verify: async (token) => {
    return new Promise((resolve, reject) => {
      jwt.verify(
        token,
        configs.token.secretKey,
        {
          algorithms: 'HS256',
        },
        (err, decoded) => {
          if (err) return reject(err);
          return resolve(decoded);
        }
      );
    });
  },
};
