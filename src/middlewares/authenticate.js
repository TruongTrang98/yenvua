const jwtMiddleware = require('./jwtMiddleware');
const UserAccountModel = require('../models/UserAccountModel');

module.exports = {
  tokenAuth: async function (req, res, next) {
    const { responses } = general;
    let accessToken = req.headers.authorization;
    if (accessToken && accessToken.includes('Bearer')) {
      accessToken = accessToken.slice(7);

      try {
        const isValidToken = await jwtMiddleware.verify(accessToken);

        if (isValidToken) {
          const { role, status } = isValidToken;
          if (role !== 'admin' && status === 'active') {
            return responses.forbiddenError(
              res,
              'Bạn không đủ quyền để thực hiện hành động này!'
            );
          }
          return next();
        }
        return responses.unauthorizedError(
          res,
          'Vui lòng đăng nhập trước khi thực hiện hành động này!'
        );
      } catch (error) {
        console.error(error.message);
        if (error.message === 'invalid token') {
          return responses.unauthorizedError(res, 'Invalid Token');
        }
        if (error.message === 'jwt expired') {
          return responses.unauthorizedError(res, 'UnAuthorized');
        }

        return responses.internalError(
          res,
          error.message || 'Some thing went wrong'
        );
      }
    }
    return responses.unauthorizedError(
      res,
      'Vui lòng đăng nhập trước khi thực hiện hành động này!'
    );
  },

  tokenUserAuth: async function (req, res, next) {
    const { responses } = general;
    let accessToken = req.headers.authorization;
    if (accessToken && accessToken.includes('Bearer')) {
      accessToken = accessToken.slice(7);

      try {
        const isValidToken = await jwtMiddleware.verify(accessToken);

        if (isValidToken) {
          if (isValidToken.status !== 'active') {
            return responses.sessionExpire(
              res,
              'Thông tin tài khoản không hơp lệ '
            );
          }
          const { phoneNumber } = isValidToken;
          const checkAccount = await UserAccountModel.findOne({
            phoneNumber,
            status: 'active',
          }).lean();
          if (!checkAccount) {
            return responses.sessionExpire(
              res,
              'Thông tin tài khoản không hơp lệ '
            );
          }
          return next();
        }

        return responses.unauthorizedError(
          res,
          'Vui lòng đăng nhập trước khi thực hiện hành động này!'
        );
      } catch (error) {
        console.error(error.message);
        if (error.message === 'invalid token') {
          return responses.unauthorizedError(res, 'Invalid Token');
        }
        if (error.message === 'jwt expired') {
          return responses.unauthorizedError(res, 'UnAuthorize');
        }
        return responses.internalError(
          res,
          error.message || 'Some thing went wrong'
        );
      }
    }
    return responses.unauthorizedError(
      res,
      'Vui lòng đăng nhập trước khi thực hiện hành động này!'
    );
  },
};
