const mongoose = require('mongoose')
const {
  configs
} = require('../configs')

const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
}

let dbIns;
const db = mongoose.connection

db.on('connecting', () => {
  console.log('Database is connecting')
})

db.on('connected', () => {
  console.log('Database connected successfully')
})

db.on('disconnected', () => {
  console.log('Database is disconnected')
})

db.on('reconnected', () => {
  console.log('Database is reconnecting')
})

async function start() {
  try {
    dbIns = await mongoose.connect(configs.mongo.uri, options)
  } catch (error) {
    console.error(error)
    throw new Error(error.message)
  }
}

async function stop() { }

module.exports = {
  start,
  stop,
  dbIns
}
