const Redis = require('ioredis');
let redisIns;

async function start() {
  redisIns = new Redis(process.env.REDIS);

  redisIns.on('ready', () => {
    console.log('Redis connected!');
  });

  redisIns.on('error', (error) => {
    console.log(`Error in redis connection: ${error}`);
  });

  redisIns.on('reconnecting', () => {
    console.log('Redis connecting...');
  });
}

async function setCache(key, value, ttlMs = 10 * 1000) {
  if (!redisIns) {
    throw new Error('Can not find redis instance!');
  }

  await redisIns.set(key, value);
  await redisIns.expire(key, parseInt(+ttlMs / 1000));
}

async function getCache(key) {
  if (!redisIns) {
    throw new Error('Can not find redis instance!');
  }

  const data = await redisIns.get(key);
  if (!data) return null;
  try {
    return JSON.parse(data);
  } catch (error) {
    return data;
  }
}

async function deleteCache(key) {
  await redisIns.del(key);
}

module.exports = { start, setCache, getCache, deleteCache };
