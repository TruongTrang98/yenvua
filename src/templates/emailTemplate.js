
module.exports = `
<h2><span style="color: #ff0000;">Bạn vừa nhận được một đơn h&agrave;ng mới!!!</span>&nbsp;<img src="https://html5-editor.net/tinymce/plugins/emoticons/img/smiley-cool.gif" alt="cool" />&nbsp;<img src="https://html5-editor.net/tinymce/plugins/emoticons/img/smiley-cool.gif" alt="cool" />&nbsp;<img src="https://html5-editor.net/tinymce/plugins/emoticons/img/smiley-cool.gif" alt="cool" /></h2>
<table class="editorDemoTable">
  <tbody>
    <tr>
      <td><strong>STT</strong></td>
      <td><strong>Sản phẩm</strong></td>
      <td><strong>Số lượng</strong></td>
      <td><strong>Đơn giá</strong></td>
      <td><strong>Tổng tiền</strong></td>
    </tr>
    <% orderInfo.forEach((item, index) => { %>
    <tr>
      <td><%= index + 1 %></td>
      <td><%= item.name %></td>
      <td><%= item.quantity %></td>
      <td><%= item.price.toLocaleString('vi', {style : 'currency', currency : 'VND'}) %></td>
      <td><%= item.total.toLocaleString('vi', {style : 'currency', currency : 'VND'}) %></td>
    </tr>
    <%}) %>
  </tbody>
</table>
  <% 
    if(note)  { %>      
      <p>
        <b>Ghi chú: </b> <%= note %>
      </p>
    <% }
  %>
<p><span style="color: #ff0000;"><strong>Tổng tiền: <%= total.toLocaleString('vi', {style : 'currency', currency : 'VND'}) %></strong></span></p>
`
