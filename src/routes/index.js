const accountController = require('../controllers/AccountController');
const addressController = require('../controllers/AddressController');
const productController = require('../controllers/ProductController');
const uploadController = require('../controllers/UploadController');
const orderController = require('../controllers/OrderController');
const categoryController = require('../controllers/CategoryController');
// const userAccountController = require('../controllers/UserAccountController');

const { authenticate, upload } = general;

module.exports = function (app) {
  // USER PAGE
  app.post('/api/v1/login', accountController.login);
  app.get('/api/v1/product', productController.getListProduct);
  app.get('/api/v1/product/detail', productController.getProductDetail);
  app.get('/api/v1/category', categoryController.getListCategory);
  app.get('/api/v1/address', addressController.getAddresses);

  // === OTP
  // app.post('/api/v1/otp/send', userAccountController.sendOTP);
  // app.post('/api/v1/otp/confirm', userAccountController.confirmOTP);
  // app.post('/api/v1/otp/updateProfile', userAccountController.updateOTPProfile);

  // ====
  // app.post('/api/v1/user/login', userAccountController.userLogin);
  // authenticate.tokenUserAuth,
  //   app.post(
  //     '/api/v1/user/updateProfile',
  //     authenticate.tokenUserAuth,
  //     userAccountController.updateProfile
  //   );

  app.post(
    '/api/v1/order',
    // authenticate.tokenUserAuth,
    orderController.createOrder
  );

  // ADMIN PAGE
  // UPLOAD IMAGE
  app.post(
    '/api/admin/v1/upload',
    authenticate.tokenAuth,
    upload.single('file'),
    uploadController.upload
  );

  // PRODUCT
  app.get(
    '/api/admin/v1/product',
    authenticate.tokenAuth,
    productController.getListProductAdmin
  );
  app.get(
    '/api/admin/v1/product/detail',
    authenticate.tokenAuth,
    productController.getProductDetailAdmin
  );
  app.put(
    '/api/admin/v1/product/status',
    authenticate.tokenAuth,
    productController.updateStatus
  );

  app
    .route('/api/admin/v1/product')
    .all(authenticate.tokenAuth)
    .post(productController.createProduct)
    .put(productController.updateProduct)
    .delete(productController.deleteProduct);

  // ORDER
  app.get(
    '/api/admin/v1/order',
    authenticate.tokenAuth,
    orderController.getListOrders
  );
  app.post(
    '/api/admin/v1/order/status',
    authenticate.tokenAuth,
    orderController.changeOrderStatus
  );
  app.get(
    '/api/admin/v1/order/detail',
    authenticate.tokenAuth,
    orderController.getOrderDetail
  );

  //CATEGORY
  app
    .route('/api/admin/v1/category')
    .all(authenticate.tokenAuth)
    .get(categoryController.getListCategoryAdmin)
    .post(categoryController.createCategory)
    .put(categoryController.updateCategory)
    .delete(categoryController.deleteCategory);

  // USER MANAGE
  // app
  //   .route('/api/admin/v1/user')
  //   .all(authenticate.tokenAuth)
  //   .get(userAccountController.getUserListAdmin)
  //   .delete(userAccountController.deleteUser);

  // app.get(
  //   '/api/admin/v1/user/reset',
  //   authenticate.tokenAuth,
  //   userAccountController.resetRegisterTimes
  // );
  // app.get(
  //   '/api/admin/v1/user/block',
  //   authenticate.tokenAuth,
  //   userAccountController.blockUser
  // );
};
