const Joi = require('joi');
const _ = require('lodash');
const {
  responses,
  hashUtils,
  jwtMiddleware,
  models: { AccountModel },
} = general;

async function login(req, res) {
  try {
    const paramSchema = Joi.object().keys({
      username: Joi.string().required(),
      password: Joi.string().required(),
    });

    const { error } = paramSchema.validate(req.body);

    if (error) {
      return responses.joiValidationError(res, error);
    }

    const { username, password } = req.body;

    const account = await AccountModel.findOne({
      username,
      status: 'active',
    }).lean();

    if (_.isEmpty(account)) {
      return responses.invalidArgumentError(
        res,
        'Không tìm thấy thông tin tài khoản!'
      );
    }

    const isPasswordMatch = await hashUtils.compare(password, account.password);

    if (!isPasswordMatch) {
      return responses.invalidArgumentError(
        res,
        'Username hoặc password không chính xác!'
      );
    }

    const accessToken = await jwtMiddleware.sign(
      global.general.constants.ACCOUNT_TYPE.ADMIN,
      _.pick(account, ['_id, username', 'role', 'status'])
    );

    delete account.password;
    delete account.role;

    return responses.success(res, {
      accessToken,
      profile: account,
    });
  } catch (error) {
    return responses.internalError(res, error);
  }
}

module.exports = {
  login,
};
