const _ = require('lodash');
const { responses, configs } = general;

module.exports = {
  upload: async function (req, res) {
    try {
      const file = req.file;

      if (!file) {
        return responses.invalidArgumentError(res, 'Please upload a file');
      }
      return responses.success(res, {
        imagePath: `${configs.express.hostName}/images/${file.filename}`,
      });
    } catch (error) {
      return responses.internalError(res, error);
    }
  },
};
