const {
  responses,
  models: { AddressModel },
} = general;

async function getAddresses(req, res) {
  try {
    let { parentCode } = req.query;

    if (!parentCode) {
      return responses.invalidArgumentError(res, 'Missing parentCode!');
    }

    const addresses = await AddressModel.find({ parentCode: +parentCode });

    return responses.success(res, {
      addresses,
    });
  } catch (error) {
    return responses.internalError(res, error);
  }
}

module.exports = {
  getAddresses,
};
