const Joi = require('joi');
const _ = require('lodash');

const {
  responses,
  generateSlug,
  removeAccents,
  models: { ProductModel, CategoryModel },
} = general;

const MAP_SORT = {
  asc: 1,
  desc: -1,
};

async function getListProduct(req, res) {
  try {
    let { page = 0, limit = 10, keyword, categoryId, sortBy } = req.query;
    page = parseInt(page);
    limit = parseInt(limit);

    if (page < 0) {
      return responses.invalidArgumentError(res, 'Page phải lớn hơn 0');
    }

    if (limit <= 0) {
      return responses.invalidArgumentError(res, 'Limit phải lớn hơn 0');
    }

    const sortCondition = {};

    if (sortBy) {
      const regex = new RegExp(/\(|\)/, 'g');
      const _sortBy = sortBy.replace(regex, '').split(':');
      if (_sortBy.length === 2) {
        if (
          ['price', 'name', 'updatedAt'].includes(_sortBy[0]) &&
          ['asc', 'desc'].includes(_sortBy[1])
        )
          sortCondition[_sortBy[0]] = MAP_SORT[_sortBy[1]];
      }
    }

    const queryCondition = {
      isDeleted: false,
      isActive: true,
    };

    if (keyword) {
      const _keyword = removeAccents(keyword);
      const _temp = _keyword.split(' ').map((item) => {
        return { slug: { $regex: item } };
      });
      queryCondition['$or'] = [{ $and: _temp }, { code: { $regex: keyword } }];
    }

    if (categoryId) {
      queryCondition.categoryId = categoryId;
    }

    const total = await ProductModel.countDocuments(queryCondition);

    const products = await ProductModel.find(queryCondition)
      .sort(sortCondition)
      .skip(page * limit)
      .limit(limit)
      .lean();

    return responses.success(res, {
      paging: {
        page,
        limit,
        total,
      },
      products,
    });
  } catch (error) {
    return responses.internalError(res, error);
  }
}

async function getListProductAdmin(req, res) {
  try {
    let { page = 0, limit = 10, keyword, categoryId, sortBy } = req.query;
    page = parseInt(page);
    limit = parseInt(limit);

    if (page < 0) {
      return responses.invalidArgumentError(res, 'Page phải lớn hơn 0');
    }

    if (limit <= 0) {
      return responses.invalidArgumentError(res, 'Limit phải lớn hơn 0');
    }

    const queryCondition = {
      isDeleted: false,
    };

    if (keyword) {
      const _keyword = removeAccents(keyword);
      const _temp = _keyword.split(' ').map((item) => {
        return { slug: { $regex: item } };
      });
      queryCondition['$or'] = [{ $and: _temp }, { code: { $regex: keyword } }];
    }

    if (categoryId) {
      queryCondition.categoryId = categoryId;
    }

    const sortCondition = {};

    if (sortBy) {
      const regex = new RegExp(/\(|\)/, 'g');
      const _sortBy = sortBy.replace(regex, '').split(':');
      if (_sortBy.length === 2) {
        if (
          ['price', 'name', 'updatedAt'].includes(_sortBy[0]) &&
          ['asc', 'desc'].includes(_sortBy[1])
        )
          sortCondition[_sortBy[0]] = MAP_SORT[_sortBy[1]];
      }
    }

    const total = await ProductModel.countDocuments(queryCondition);

    const products = await ProductModel.find(queryCondition)
      .sort(sortCondition)
      .skip(page * limit)
      .limit(limit)
      .lean();

    return responses.success(res, {
      paging: {
        page,
        limit,
        total,
      },
      products,
    });
  } catch (error) {
    return responses.internalError(res, error);
  }
}

async function getListProductBestSale(req, res) {
  try {
    const queryCondition = {
      isDeleted: false,
      isActive: true,
    };

    const sortCondition = {
      saleNumber: -1,
    };
    const products = await ProductModel.find(queryCondition)
      .sort(sortCondition)
      .limit(10)
      .lean();

    return responses.success(res, {
      products,
    });
  } catch (error) {
    return responses.internalError(res, error);
  }
}

async function getProductDetail(req, res) {
  try {
    const paramSchema = Joi.object({
      code: Joi.string().required(),
    });

    const { error, value } = paramSchema.validate(req.query);

    if (error) {
      return responses.joiValidationError(res, error);
    }

    const product = await ProductModel.findOne({
      ...value,
      isDeleted: false,
      isActive: true,
    }).lean();

    if (!product) {
      return responses.invalidArgumentError(res, {
        message: 'Không tìm thấy thông tin sản phẩm',
      });
    }

    return responses.success(res, {
      product,
    });
  } catch (error) {
    return responses.internalError(res, error);
  }
}

async function getProductDetailAdmin(req, res) {
  try {
    const paramSchema = Joi.object({
      code: Joi.string().required(),
    });

    const { error, value } = paramSchema.validate(req.query);

    if (error) {
      return responses.joiValidationError(res, error);
    }

    const product = await ProductModel.findOne({
      ...value,
      isDeleted: false,
    }).lean();
    if (!product) {
      return responses.invalidArgumentError(res, {
        message: 'Không tìm thấy thông tin sản phẩm',
      });
    }

    return responses.success(res, {
      product,
    });
  } catch (error) {
    return responses.internalError(res, error);
  }
}

async function createProduct(req, res) {
  try {
    const paramSchema = Joi.object({
      code: Joi.string().required(),
      name: Joi.string().required(),
      image: Joi.string().required(),
      categoryId: Joi.string().required(),
      description: Joi.string()
        .optional()
        .default('Chưa có mô tả cho sản phẩm này'),
      shortDescription: Joi.string().optional(),
      price: Joi.number().required().min(0),
      isActive: Joi.boolean().default(true),
    });

    const { error, value } = paramSchema.validate(req.body);

    if (error) {
      return responses.joiValidationError(res, error);
    }

    const isExistedCategory = await CategoryModel.findOne({
      _id: value.categoryId,
      isDeleted: false,
    }).lean();
    if (!isExistedCategory) {
      return responses.invalidArgumentError(
        res,
        'Không tim thấy thông tin danh mục!'
      );
    }

    value.slug = generateSlug(value.name) + '-' + Date.now();
    const isExistedCode = await ProductModel.findOne({
      code: value.code,
      isDeleted: false,
    }).lean();
    if (isExistedCode) {
      return responses.invalidArgumentError(res, 'Mã sản phẩm đã tồn tại!');
    }

    await ProductModel.create(value);
    return responses.success(res, {
      message: 'Tạo sản phẩm thành công!',
    });
  } catch (error) {
    return responses.internalError(res, error);
  }
}

async function updateProduct(req, res) {
  try {
    let paramSchema = Joi.object({
      name: Joi.string().optional(),
      image: Joi.string().optional(),
      categoryId: Joi.string().optional(),
      description: Joi.string().optional(),
      shortDescription: Joi.string().optional(),
      price: Joi.number().optional().min(0),
    });

    const { error, value } = paramSchema.validate(req.body);

    if (error) {
      return responses.joiValidationError(res, error);
    }

    paramSchema = Joi.object({
      code: Joi.string().required(),
    });

    const validProductSlug = paramSchema.validate(req.query);

    if (validProductSlug.error) {
      return responses.joiValidationError(res, validProductSlug.error);
    }

    const { code } = req.query;

    const isProductExisted = await ProductModel.findOne({
      code,
      isDeleted: false,
    })
      .select('_id')
      .lean();
    if (!isProductExisted) {
      return responses.invalidArgumentError(
        res,
        'Không tìm thấy thông tin sản phẩm'
      );
    }

    if (Object.keys(value).length <= 0) {
      return responses.success(res, {
        message: 'Cập nhật thông tin sản phẩm thành công!',
      });
    }

    const updated = await ProductModel.findOneAndUpdate(
      { code, isDeleted: false },
      {
        $set: value,
      },
      { new: true }
    );

    return responses.success(res, {
      product: updated,
    });
  } catch (error) {
    return responses.internalError(res, error);
  }
}

async function updateStatus(req, res) {
  try {
    const paramSchema = Joi.object({
      code: Joi.string().required(),
    });

    const {
      error,
      value: { code },
    } = paramSchema.validate(req.query);

    if (error) {
      return responses.joiValidationError(res, error);
    }

    const productInfo = await ProductModel.findOne({
      code,
      isDeleted: false,
    })
      .select('isActive')
      .lean();

    if (!productInfo) {
      return responses.invalidArgumentError(
        res,
        'Không tìm thấy thông tin sản phẩm'
      );
    }

    await ProductModel.updateOne(
      {
        code,
        isDeleted: false,
      },
      {
        $set: {
          isActive: !productInfo.isActive,
        },
      }
    );

    return responses.success(res, {
      message: 'Cập nhật trạng thái thành công!',
    });
  } catch (error) {
    return responses.internalError(res, error);
  }
}

async function deleteProduct(req, res) {
  try {
    const paramSchema = Joi.object({
      code: Joi.string().required(),
    });

    const {
      error,
      value: { code },
    } = paramSchema.validate(req.query);

    if (error) {
      return responses.joiValidationError(res, error);
    }

    const productInfo = await ProductModel.findOne({
      code,
      isDeleted: false,
    })
      .select('isActive')
      .lean();

    if (!productInfo) {
      return responses.invalidArgumentError(
        res,
        'Không tìm thấy thông tin sản phẩm'
      );
    }

    await ProductModel.updateOne(
      {
        code,
        isDeleted: false,
      },
      {
        $set: {
          isDeleted: true,
        },
      }
    );

    return responses.success(res, {
      message: 'Đã xóa sản phẩm!',
    });
  } catch (error) {
    return responses.internalError(res, error);
  }
}

module.exports = {
  getListProduct,
  getListProductAdmin,
  getListProductBestSale,
  getProductDetail,
  getProductDetailAdmin,
  createProduct,
  updateProduct,
  updateStatus,
  deleteProduct,
};
