// const Joi = require('joi');
// const AddressModel = require('../models/AddressModel');
// const _ = require('lodash');
// const validAddressCode = require('../constant/validAddressCode');
// const {
//   responses,
//   hashUtils,
//   jwtMiddleware,
//   isVietnamesePhoneNumber,
//   generateOTP,
//   models: { UserAccountModel },
//   // cache: { cacheIns },
// } = general;

// const MAP_SORT = {
//   asc: 1,
//   desc: -1,
// };

// async function sendOTP(req, res) {
//   try {
//     const paramSchema = Joi.object().keys({
//       phoneNumber: Joi.string().required(),
//     });

//     const { error } = paramSchema.validate(req.body);

//     if (error) {
//       return responses.joiValidationError(res, error);
//     }

//     const { phoneNumber } = req.body;
//     if (!isVietnamesePhoneNumber(phoneNumber)) {
//       return responses.joiValidationError(
//         res,
//         error,
//         'Số điện thoại không hợp lệ!'
//       );
//     }

//     // SENT OTP
//     // Check PHONE NUMBER EXISTED OR NOT AND BLOCK OR NOT
//     let phoneInvalid;

//     phoneInvalid = await UserAccountModel.findOne({
//       phoneNumber,
//       status: 'active',
//     }).lean();

//     if (phoneInvalid) {
//       return responses.invalidArgumentError(
//         res,
//         'Số điện thoại đã đăng ký tài khoản!'
//       );
//     }

//     phoneInvalid = await UserAccountModel.findOne({
//       phoneNumber,
//       status: 'inactive',
//     }).lean();

//     if (phoneInvalid) {
//       return responses.invalidArgumentError(
//         res,
//         'Tài khoản đang bị khóa. Vui lòng liên hệ admin để được hỗ trợ!'
//       );
//     }

//     phoneInvalid = await UserAccountModel.findOne({
//       phoneNumber,
//       status: 'pending',
//     });

//     if (phoneInvalid) {
//       if (phoneInvalid.registerTimes >= 3) {
//         return responses.invalidArgumentError(
//           res,
//           'Số điện thoại đã đăng ký sai quá số lần cho phép. Vui lòng liên hệ admin để được hỗ trợ!'
//         );
//       } else {
//         phoneInvalid.registerTimes = phoneInvalid.registerTimes + 1;
//         await phoneInvalid.save();
//       }
//     } else {
//       await UserAccountModel.create({ phoneNumber });
//     }

//     const otp = generateOTP();
//     console.log('🚀 ~ file: UserAccountController.js ~ line 87 ~ otp', otp);

//     if (process.env.NODE_ENV === 'dev') {
//       await cacheIns.setCache(
//         `otp:${phoneNumber}`,
//         otp,
//         +process.env.OTP_TTL_MS * 1000
//       );
//     } else {
//       await cacheIns.setCache(
//         `otp:${phoneNumber}`,
//         otp,
//         +process.env.OTP_TTL_MS * 1000
//       );
//     }

//     return responses.success(res, {
//       message: 'Đã gửi mã OTP!',
//     });
//   } catch (error) {
//     return responses.internalError(res, error);
//   }
// }

// async function confirmOTP(req, res) {
//   try {
//     const paramSchema = Joi.object().keys({
//       phoneNumber: Joi.string().required(),
//       otp: Joi.string(),
//     });

//     const { error } = paramSchema.validate(req.body);

//     if (error) {
//       return responses.joiValidationError(res, error);
//     }

//     const { phoneNumber, otp } = req.body;
//     console.log('🚀 ~ file: UserAccountController.js ~ line 83 ~ otp', otp);

//     const otpSaved = await cacheIns.getCache(`otp:${phoneNumber}`);

//     if (!otpSaved) {
//       return responses.invalidArgumentError(res, 'OTP ko hợp lệ!');
//     }

//     if (otpSaved != otp) {
//       return responses.invalidArgumentError(res, 'OTP ko hợp lệ!');
//     }

//     await cacheIns.deleteCache(`otp:${phoneNumber}`);
//     await cacheIns.setCache(
//       `otp:${phoneNumber}:profile`,
//       1,
//       +process.env.OTP_TTL_MS * 1000
//     );

//     return responses.success(res, {
//       message: 'CONFIRMED!',
//     });
//   } catch (error) {
//     return responses.internalError(res, error);
//   }
// }

// async function updateOTPProfile(req, res) {
//   try {
//     const paramSchema = Joi.object().keys({
//       phoneNumber: Joi.string().required(),
//       email: Joi.string().email(),
//       name: Joi.string().trim().required(),
//       address: Joi.object({
//         city: Joi.object({
//           code: Joi.number()
//             .valid(...validAddressCode.city)
//             .required(),
//           codename: Joi.string().required(),
//           name: Joi.string().required(),
//         }).required(),
//         district: Joi.object({
//           code: Joi.number()
//             .valid(...validAddressCode.district)
//             .required(),
//           codename: Joi.string().required(),
//           name: Joi.string().required(),
//         }).required(),
//         ward: Joi.object({
//           code: Joi.number()
//             .valid(...validAddressCode.ward)
//             .required(),
//           codename: Joi.string().required(),
//           name: Joi.string().required(),
//         }).required(),
//         street: Joi.string().required(),
//       }).required(),
//       password: Joi.string().required(),
//       gender: Joi.string().required().valid('MALE', 'FEMALE', 'OTHER'),
//       confirmPassword: Joi.string().required(),
//     });

//     const { error } = paramSchema.validate(req.body);

//     if (error) {
//       return responses.joiValidationError(res, error);
//     }

//     const {
//       phoneNumber,
//       email,
//       name,
//       address,
//       password,
//       gender,
//       confirmPassword,
//     } = req.body;

//     //handle password vs confirmPassword
//     if (password !== confirmPassword) {
//       return responses.invalidArgumentError(
//         res,
//         'Password và Xác nhận password không trùng khớp!'
//       );
//     }

//     // Check valid address
//     const {
//       city: { code: cityCode, codename: cityCodename, name: cityName },
//       district: {
//         code: districtCode,
//         codename: districtCodename,
//         name: districtName,
//       },
//       ward: { code: wardCode, codename: wardCodename, name: wardName },
//     } = address;

//     let addresses = await AddressModel.find({
//       code: { $in: [cityCode, districtCode, wardCode] },
//     });

//     if (_.isEmpty(addresses) || addresses.length !== 3) {
//       return responses.invalidArgumentError(res, 'Sai thông tin địa chỉ');
//     }

//     addresses = _.reduce(
//       addresses,
//       function (acc, cur) {
//         acc[cur.code] = cur;
//         return acc;
//       },
//       {}
//     );

//     console.log(
//       '🚀 ~ file: UserAccountController.js ~ line 251 ~ addresses',
//       addresses
//     );

//     let flagWrongAddress = false;
//     console.log(
//       '🚀 ~ file: UserAccountController.js ~ line 269 ~ flagWrongAddress',
//       flagWrongAddress
//     );
//     Object.keys(addresses).forEach((item) => {
//       switch (+item) {
//         case cityCode: {
//           if (addresses[item].codename !== cityCodename) {
//             flagWrongAddress = true;
//           }
//           if (addresses[item].name !== cityName) {
//             flagWrongAddress = true;
//           }
//           break;
//         }
//         case districtCode: {
//           if (addresses[item].codename !== districtCodename) {
//             flagWrongAddress = true;
//           }
//           if (addresses[item].name !== districtName) {
//             flagWrongAddress = true;
//           }
//           break;
//         }
//         case wardCode: {
//           if (addresses[item].codename !== wardCodename) {
//             flagWrongAddress = true;
//           }
//           if (addresses[item].name !== wardName) {
//             flagWrongAddress = true;
//           }
//           break;
//         }
//       }
//     });

//     console.log(
//       '🚀 ~ file: UserAccountController.js ~ line 303 ~ flagWrongAddress',
//       flagWrongAddress
//     );

//     if (flagWrongAddress) {
//       return responses.invalidArgumentError(res, 'Sai thông tin địa chỉ');
//     }

//     const existedAccount = await UserAccountModel.findOne({
//       phoneNumber,
//       status: 'active',
//     }).lean();
//     console.log(
//       '🚀 ~ file: UserAccountController.js ~ line 296 ~ existedAccount',
//       existedAccount
//     );

//     if (existedAccount) {
//       return responses.invalidArgumentError(
//         res,
//         'Số điện thoại đã đăng ký rồi!'
//       );
//     }

//     const checkValidPhoneNumber = await cacheIns.getCache(
//       `otp:${phoneNumber}:profile`
//     );

//     if (!checkValidPhoneNumber) {
//       return responses.invalidArgumentError(
//         res,
//         'Số điện thoại chưa được đăng ký!'
//       );
//     }

//     await cacheIns.deleteCache(`otp:${phoneNumber}:profile`);

//     const hashedPassword = await hashUtils.hash(password);

//     await UserAccountModel.updateOne(
//       {
//         phoneNumber,
//       },
//       {
//         email,
//         name,
//         address,
//         password: hashedPassword,
//         gender,
//         status: 'active',
//       }
//     );

//     return responses.success(res, {
//       message: 'Tạo tài khoản thành công!',
//     });
//   } catch (error) {
//     return responses.internalError(res, error);
//   }
// }

// async function updateProfile(req, res) {
//   try {
//     const paramSchema = Joi.object().keys({
//       phoneNumber: Joi.string().optional(),
//       email: Joi.string().email().optional(),
//       name: Joi.string().trim().optional(),
//       address: Joi.string().optional(), // How to check address valid
//       gender: Joi.string().optional().valid('MALE', 'FEMALE', 'OTHER'),
//     });

//     const { error } = paramSchema.validate(req.body);

//     if (error) {
//       return responses.joiValidationError(res, error);
//     }

//     const { phoneNumber } = req.body;

//     const updateValue = _.pickBy(req.body, function (value, key) {
//       return !(value === undefined || key === 'phoneNumber');
//     });

//     console.log(
//       '🚀 ~ file: UserAccountController.js ~ line 233 ~ updateValue',
//       updateValue
//     );

//     const checkExisted = await UserAccountModel.findOne({
//       phoneNumber,
//       status: 'active',
//     });

//     if (!checkExisted) {
//       return responses.invalidArgumentError(
//         res,
//         'Không tìm thấy thông tin tài khoản!'
//       );
//     }

//     await UserAccountModel.updateOne(
//       {
//         phoneNumber,
//       },
//       {
//         $set: updateValue,
//       }
//     );

//     return responses.success(res, {
//       message: 'Cập nhật tài khoản thành công!',
//     });
//   } catch (error) {
//     return responses.internalError(res, error);
//   }
// }

// async function userLogin(req, res) {
//   try {
//     const paramSchema = Joi.object().keys({
//       phoneNumber: Joi.string().required(),
//       password: Joi.string().required(),
//     });

//     const { error } = paramSchema.validate(req.body);

//     if (error) {
//       return responses.joiValidationError(res, error);
//     }

//     const { phoneNumber, password } = req.body;

//     const account = await UserAccountModel.findOne({
//       phoneNumber,
//       status: 'active',
//     }).lean();

//     if (_.isEmpty(account)) {
//       return responses.invalidArgumentError(
//         res,
//         'Không tìm thấy thông tin tài khoản!'
//       );
//     }

//     const isPasswordMatch = await hashUtils.compare(password, account.password);

//     if (!isPasswordMatch) {
//       return responses.invalidArgumentError(
//         res,
//         'Username hoặc password không chính xác!'
//       );
//     }

//     const accessToken = await jwtMiddleware.sign(
//       global.general.constants.ACCOUNT_TYPE.USER,
//       _.pick(account, ['_id', 'phoneNumber', 'status'])
//     );

//     delete account.password;
//     delete account.address;
//     delete account.registerTimes;

//     return responses.success(res, {
//       accessToken,
//       profile: account,
//     });
//   } catch (error) {
//     return responses.internalError(res, error);
//   }
// }

// async function getUserListAdmin(req, res) {
//   try {
//     let { page = 0, limit = 10, keyword, sortBy } = req.query;
//     page = parseInt(page);
//     limit = parseInt(limit);

//     if (page < 0) {
//       return responses.invalidArgumentError(res, 'Page phải lớn hơn 0');
//     }

//     if (limit <= 0) {
//       return responses.invalidArgumentError(res, 'Limit phải lớn hơn 0');
//     }

//     const queryCondition = {};

//     if (keyword) {
//       queryCondition['$or'] = [
//         {
//           phoneNumber: { $regex: keyword },
//         },
//         {
//           name: { $regex: keyword },
//         },
//         {
//           email: { $regex: keyword },
//         },
//       ];
//     }

//     const sortCondition = {};

//     if (sortBy) {
//       const regex = new RegExp(/\(|\)/, 'g');
//       const _sortBy = sortBy.replace(regex, '').split(':');
//       if (_sortBy.length === 2) {
//         if (
//           ['status', 'updatedAt'].includes(_sortBy[0]) &&
//           ['asc', 'desc'].includes(_sortBy[1])
//         )
//           sortCondition[_sortBy[0]] = MAP_SORT[_sortBy[1]];
//       }
//     }

//     const total = await UserAccountModel.countDocuments(queryCondition);

//     const users = await UserAccountModel.find(queryCondition)
//       .sort(sortCondition)
//       .skip(page * limit)
//       .limit(limit)
//       .lean();

//     return responses.success(res, {
//       paging: {
//         page,
//         limit,
//         total,
//       },
//       users,
//     });
//   } catch (error) {
//     return responses.internalError(res, error);
//   }
// }

// async function blockUser(req, res) {
//   try {
//     const paramSchema = Joi.object().keys({
//       phoneNumber: Joi.string().required(),
//     });

//     const { error } = paramSchema.validate(req.query);

//     if (error) {
//       return responses.joiValidationError(res, error);
//     }

//     const { phoneNumber } = req.query;

//     const account = await UserAccountModel.findOne({
//       phoneNumber,
//       status: 'active',
//     }).lean();

//     if (_.isEmpty(account)) {
//       return responses.invalidArgumentError(
//         res,
//         'Không tìm thấy thông tin tài khoản!'
//       );
//     }

//     await UserAccountModel.updateOne(
//       {
//         phoneNumber,
//       },
//       {
//         status: 'inactive',
//       }
//     );

//     return responses.success(res, {
//       message: 'Đã khóa thông tin tài khoản',
//     });
//   } catch (error) {
//     return responses.internalError(res, error);
//   }
// }

// async function resetRegisterTimes(req, res) {
//   try {
//     const paramSchema = Joi.object().keys({
//       phoneNumber: Joi.string().required(),
//     });

//     const { error } = paramSchema.validate(req.query);

//     if (error) {
//       return responses.joiValidationError(res, error);
//     }

//     const { phoneNumber } = req.query;

//     const account = await UserAccountModel.findOne({
//       phoneNumber,
//       status: 'active',
//     }).lean();

//     if (_.isEmpty(account)) {
//       return responses.invalidArgumentError(
//         res,
//         'Không tìm thấy thông tin tài khoản!'
//       );
//     }

//     await UserAccountModel.updateOne(
//       {
//         phoneNumber,
//       },
//       {
//         registerTimes: 0,
//       }
//     );

//     return responses.success(res, {
//       message: 'Cập nhật thành công!',
//     });
//   } catch (error) {
//     return responses.internalError(res, error);
//   }
// }

// async function deleteUser(req, res) {
//   try {
//     const paramSchema = Joi.object().keys({
//       phoneNumber: Joi.string().required(),
//     });

//     const { error } = paramSchema.validate(req.query);

//     if (error) {
//       return responses.joiValidationError(res, error);
//     }

//     const { phoneNumber } = req.query;

//     const account = await UserAccountModel.findOne({
//       phoneNumber,
//       status: 'active',
//     }).lean();

//     if (_.isEmpty(account)) {
//       return responses.invalidArgumentError(
//         res,
//         'Không tìm thấy thông tin tài khoản!'
//       );
//     }

//     await UserAccountModel.deleteOne({
//       phoneNumber,
//     });

//     return responses.success(res, {
//       message: 'Xóa thành công!',
//     });
//   } catch (error) {
//     return responses.internalError(res, error);
//   }
// }

// module.exports = {
//   sendOTP,
//   confirmOTP,
//   updateOTPProfile,
//   updateProfile,
//   userLogin,
//   getUserListAdmin,
//   blockUser,
//   resetRegisterTimes,
//   deleteUser,
// };
