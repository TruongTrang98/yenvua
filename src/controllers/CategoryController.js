const Joi = require('joi');
const _ = require('lodash');

const {
  responses,
  models: { ProductModel, CategoryModel },
} = general;

async function getListCategory(req, res) {
  try {
    let { page = 0, limit = 10, keyword } = req.query;
    page = parseInt(page);
    limit = parseInt(limit);

    if (page < 0) {
      return responses.invalidArgumentError(res, 'Page phải lớn hơn 0');
    }

    if (limit <= 0) {
      return responses.invalidArgumentError(res, 'Limit phải lớn hơn 0');
    }

    const queryCondition = {
      isDeleted: false,
    };
    if (keyword) {
      queryCondition.name = {
        $regex: keyword,
      };
    }

    const total = await CategoryModel.countDocuments(queryCondition);

    const categories = await CategoryModel.find(queryCondition)
      .skip(page * limit)
      .limit(limit)
      .lean();

    return responses.success(res, {
      paging: {
        page,
        limit,
        total,
      },
      categories,
    });
  } catch (error) {
    return responses.internalError(res, error);
  }
}

async function getListCategoryAdmin(req, res) {
  try {
    let { page = 0, limit = 10, keyword } = req.query;
    page = parseInt(page);
    limit = parseInt(limit);

    if (page < 0) {
      return responses.invalidArgumentError(res, 'Page phải lớn hơn 0');
    }

    if (limit <= 0) {
      return responses.invalidArgumentError(res, 'Limit phải lớn hơn 0');
    }

    const queryCondition = {
      isDeleted: false,
    };
    if (keyword) {
      queryCondition.name = {
        $regex: keyword,
      };
    }

    const total = await CategoryModel.countDocuments(queryCondition);

    const categories = await CategoryModel.find(queryCondition)
      .skip(page * limit)
      .limit(limit)
      .lean();

    return responses.success(res, {
      paging: {
        page,
        limit,
        total,
      },
      categories,
    });
  } catch (error) {
    return responses.internalError(res, error);
  }
}

async function createCategory(req, res) {
  try {
    const paramSchema = Joi.object({
      name: Joi.string().required(),
      image: Joi.string().required(),
    });

    const { error, value } = paramSchema.validate(req.body);

    if (error) {
      return responses.joiValidationError(res, error);
    }

    await CategoryModel.create(value);
    return responses.success(res, {
      message: 'Tạo danh mục sản phẩm thành công!',
    });
  } catch (error) {
    return responses.internalError(res, error);
  }
}

async function updateCategory(req, res) {
  try {
    let paramSchema = Joi.object({
      name: Joi.string().optional(),
    });

    let { error, value } = paramSchema.validate(req.body);

    if (error) {
      return responses.joiValidationError(res, error);
    }

    paramSchema = Joi.object({
      categoryId: Joi.string().required(),
    });

    const validCategoryId = paramSchema.validate(req.query);

    if (validCategoryId.error) {
      return responses.joiValidationError(res, validCategoryId.error);
    }

    const { categoryId } = req.query;

    const isCategoryExisted = await CategoryModel.findOne({
      _id: categoryId,
      isDeleted: false,
    })
      .select('_id')
      .lean();
    if (!isCategoryExisted) {
      return responses.invalidArgumentError(
        res,
        'Không tìm thấy thông tin danh mục!'
      );
    }

    if (Object.keys(value).length <= 0) {
      return responses.success(res, {
        message: 'Cập nhật thông tin danh mục thành công!',
      });
    }

    const updated = await CategoryModel.findOneAndUpdate(
      { _id: categoryId, isDeleted: false },
      {
        $set: value,
      },
      { new: true }
    );

    return responses.success(res, {
      category: updated,
    });
  } catch (error) {
    return responses.internalError(res, error);
  }
}

async function deleteCategory(req, res) {
  try {
    const paramSchema = Joi.object({
      categoryId: Joi.string().required(),
    });

    const {
      error,
      value: { categoryId },
    } = paramSchema.validate(req.query);

    if (error) {
      return responses.joiValidationError(res, error);
    }

    const categoryInfo = await CategoryModel.findOne({
      _id: categoryId,
      isDeleted: false,
    })
      .select('_id')
      .lean();

    if (!categoryInfo) {
      return responses.invalidArgumentError(
        res,
        'Không tìm thấy thông tin danh mục'
      );
    }

    await CategoryModel.updateOne(
      {
        _id: categoryId,
        isDeleted: false,
      },
      {
        $set: {
          isDeleted: true,
        },
      }
    );

    return responses.success(res, {
      message: 'Đã xóa sản phẩm!',
    });
  } catch (error) {
    return responses.internalError(res, error);
  }
}

module.exports = {
  getListCategory,
  getListCategoryAdmin,
  createCategory,
  updateCategory,
  deleteCategory,
};
