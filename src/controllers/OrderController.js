const Joi = require('joi');
const _ = require('lodash');
const { isValidEmail } = require('../utils/validateEmail');
const isVietnamesePhoneNumber = require('../utils/isValidPhone');
const validAddressCode = require('../constant/validAddressCode');
const AddressModel = require('../models/AddressModel');

const {
  responses,
  constants,
  configs,
  mailUtils,
  models: { OrderModel, ProductModel, UserAccountModel },
} = general;

const MAP_SORT = {
  asc: 1,
  desc: -1,
};

async function createOrder(req, res) {
  try {
    const paramSchema = Joi.object({
      customerInfo: Joi.object({
        name: Joi.string().required(),
        gender: Joi.string().required().valid('MALE', 'FEMALE', 'OTHER'),
        phone: Joi.string().required(),
        email: Joi.string().required(),
        address: Joi.object({
          city: Joi.object({
            code: Joi.number()
              .valid(...validAddressCode.city)
              .required(),
            codename: Joi.string().required(),
            name: Joi.string().required(),
          }).required(),
          district: Joi.object({
            code: Joi.number()
              .valid(...validAddressCode.district)
              .required(),
            codename: Joi.string().required(),
            name: Joi.string().required(),
          }).required(),
          ward: Joi.object({
            code: Joi.number()
              .valid(...validAddressCode.ward)
              .required(),
            codename: Joi.string().required(),
            name: Joi.string().required(),
          }).required(),
          street: Joi.string().required(),
        }).required(),
      }).required(),
      // userId: Joi.string().required(),
      orderInfo: Joi.array()
        .items({
          productId: Joi.string().required(),
          quantity: Joi.number().required(),
        })
        .required(),
      note: Joi.string().optional(),
    });

    const {
      error,
      value: { customerInfo, orderInfo, note },
    } = paramSchema.validate(req.body);

    if (error) {
      return responses.joiValidationError(res, error);
    }

    const { phone, email, address } = customerInfo;
    if (!isValidEmail(email)) {
      return responses.invalidArgumentError(res, 'Email không hợp lệ!');
    }

    if (!isVietnamesePhoneNumber(phone)) {
      return responses.invalidArgumentError(res, 'Số điện thoại không hợp lệ!');
    }

    // Check valid address
    const {
      city: { code: cityCode, codename: cityCodename, name: cityName },
      district: {
        code: districtCode,
        codename: districtCodename,
        name: districtName,
      },
      ward: { code: wardCode, codename: wardCodename, name: wardName },
    } = address;

    let addresses = await AddressModel.find({
      code: { $in: [cityCode, districtCode, wardCode] },
    });

    if (_.isEmpty(addresses) || addresses.length !== 3) {
      return responses.invalidArgumentError(res, 'Sai thông tin địa chỉ');
    }

    addresses = _.reduce(
      addresses,
      function (acc, cur) {
        acc[cur.code] = cur;
        return acc;
      },
      {}
    );

    let flagWrongAddress = false;

    Object.keys(addresses).forEach((item) => {
      switch (+item) {
        case cityCode: {
          if (addresses[item].codename !== cityCodename) {
            flagWrongAddress = true;
          }
          if (addresses[item].name !== cityName) {
            flagWrongAddress = true;
          }
          break;
        }
        case districtCode: {
          if (addresses[item].codename !== districtCodename) {
            flagWrongAddress = true;
          }
          if (addresses[item].name !== districtName) {
            flagWrongAddress = true;
          }
          break;
        }
        case wardCode: {
          if (addresses[item].codename !== wardCodename) {
            flagWrongAddress = true;
          }
          if (addresses[item].name !== wardName) {
            flagWrongAddress = true;
          }
          break;
        }
      }
    });

    if (flagWrongAddress) {
      return responses.invalidArgumentError(res, 'Sai thông tin địa chỉ');
    }

    // const validUser = await UserAccountModel.findOne({
    //   _id: userId,
    //   status: 'active',
    // }).lean();

    // if (!validUser) {
    //   return responses.invalidArgumentError(
    //     res,
    //     'Thông tin tài khoản không hợp lệ!'
    //   );
    // }

    const _orderInfo = [];
    const sendMailInfo = [];
    const _temp = [];
    for (const item of orderInfo) {
      const product = await ProductModel.findOne({
        _id: item.productId,
      });
      if (!product) {
        return responses.invalidArgumentError(
          res,
          'Sản phẩm đặt hàng không hợp lệ!'
        );
      }
      sendMailInfo.push({
        _id: product._id,
        name: product.name,
        quantity: item.quantity,
        price: product.price,
        total: item.quantity * product.price,
      });
      _orderInfo.push({
        productId: item.productId,
        quantity: item.quantity,
        price: product.price,
        total: item.quantity * product.price,
      });
      _temp.push({ product, quantity: item.quantity });
    }

    const totalPriceOfOrder = _orderInfo.reduce((acc, cur) => {
      return acc + cur.total;
    }, 0);

    const newOrder = {
      customerInfo: customerInfo,
      orderInfo: _orderInfo,
      status: constants.ORDER_STATUS.CREATED,
      note,
      total: totalPriceOfOrder,
    };

    if (configs.mailService.isSendMail) {
      mailUtils.sendMail({
        targetEmail: customerInfo.email,
        orderInfo: sendMailInfo,
        note,
        total: totalPriceOfOrder,
      });
    }

    await OrderModel.create(newOrder);

    _temp.map(async (item) => {
      item.product.saleNumber = item.product.saleNumber + item.quantity;
      await item.product.save();
    });

    return responses.success(res, {
      message: 'Đặt hàng thành công!',
    });
  } catch (error) {
    return responses.internalError(res, error);
  }
}

async function getListOrders(req, res) {
  try {
    let { page = 0, limit = 10, keyword, status, sortBy } = req.query;
    page = parseInt(page);
    limit = parseInt(limit);

    if (page < 0) {
      return responses.invalidArgumentError(res, 'Page phải lớn hơn 0');
    }

    if (limit <= 0) {
      return responses.invalidArgumentError(res, 'Limit phải lớn hơn 0');
    }

    const queryCondition = {};
    if (keyword) {
      queryCondition['$or'] = [];
    }

    if (status && ['CREATED', 'DONE', 'CANCELED'].includes(status)) {
      queryCondition.status = status;
    }

    const sortCondition = {};

    if (sortBy) {
      const regex = new RegExp(/\(|\)/, 'g');
      const _sortBy = sortBy.replace(regex, '').split(':');
      if (_sortBy.length === 2) {
        if (
          ['total', 'createdAt'].includes(_sortBy[0]) &&
          ['asc', 'desc'].includes(_sortBy[1])
        )
          sortCondition[_sortBy[0]] = MAP_SORT[_sortBy[1]];
      }
    }

    const total = await OrderModel.countDocuments(queryCondition);

    const orders = await OrderModel.find(queryCondition)
      .sort(sortCondition)
      .skip(page * limit)
      .limit(limit)
      .populate({
        path: 'orderInfo.productId',
        select: '_id slug name image',
      })
      .lean();

    return responses.success(res, {
      paging: {
        page,
        limit,
        total,
      },
      orders,
    });
  } catch (error) {
    return responses.internalError(res, error);
  }
}

async function getOrderDetail(req, res) {
  try {
    const paramSchema = Joi.object({
      orderId: Joi.string().required(),
    });

    const {
      error,
      value: { orderId },
    } = paramSchema.validate(req.query);

    if (error) {
      return responses.joiValidationError(res, error);
    }

    const order = await OrderModel.findOne({ _id: orderId })
      .populate({
        path: 'orderInfo.productId',
        select: '_id slug name image',
      })
      .lean();

    if (!order) {
      return responses.success(res, {
        message: 'Không tìm thấy thông tin đặt hàng',
      });
    }

    return responses.success(res, {
      order,
    });
  } catch (error) {
    return responses.internalError(res, error);
  }
  s;
}

async function changeOrderStatus(req, res) {
  const validStatus = {
    CREATED: ['DONE', 'CANCELED'],
    DONE: ['CANCELED'],
    CANCELED: [],
  };
  try {
    const paramSchema = Joi.object({
      orderId: Joi.string().required(),
      status: Joi.string().valid('CREATED', 'DONE', 'CANCELED').required(),
    });

    const {
      error,
      value: { orderId, status },
    } = paramSchema.validate(req.body);

    if (error) {
      return responses.joiValidationError(res, error);
    }

    const order = await OrderModel.findOne({ _id: orderId }).lean();

    if (!order) {
      return responses.invalidArgumentError(
        res,
        'Không tìm thấy thông tin đơn hàng!'
      );
    }

    if (!validStatus[order.status].includes(status)) {
      return responses.invalidArgumentError(
        res,
        'Trạng thái mới không hợp lệ!'
      );
    }

    await OrderModel.updateOne(
      {
        _id: orderId,
      },
      {
        status,
      }
    );

    return responses.success(res, {
      message: 'Cập nhật trạng thái đơn hàng thành công!',
    });
  } catch (error) {
    return responses.internalError(res, error);
  }
}

module.exports = {
  createOrder,
  getListOrders,
  getOrderDetail,
  changeOrderStatus,
};
