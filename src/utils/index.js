const hashUtils = require('./hashUtils');
const generateSlug = require('./generateSlug');
const mailUtils = require('./mailUtils');
const removeAccents = require('./removeAccents');
const isVietnamesePhoneNumber = require('./isValidPhone');
const generateOTP = require('./generateOTP');

module.exports = {
  hashUtils,
  mailUtils,
  generateSlug,
  removeAccents,
  isVietnamesePhoneNumber,
  generateOTP,
};
