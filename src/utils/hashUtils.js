/** @module utils/hash */
const bcrypt = require('bcryptjs')
const crypto = require('crypto')
const saltRounds = 10

module.exports = {
  /**
   * Bcrypt Hash Function wrapper
   * @function
   * @param {string} text
   * @return {promise} A promise which resolved to the hash result
   */
  hash: function (text) {
    return new Promise((resolve, reject) => {
      bcrypt.hash(text, saltRounds, function (err, hashValue) {
        if (err) {
          reject(err)
        } else {
          resolve(hashValue)
        }
      })
    })
  },
  /**
   * Bcrypt Hash Compare Function wrapper
   * @param {string} rawValue
   * @param {string} hashValue
   * @return {promise} A promise which resolved to compare result
   */
  compare: function (rawValue, hashValue) {
    return new Promise((resolve, reject) => {
      bcrypt.compare(rawValue, hashValue, function (err, res) {
        if (err) {
          // do something
          reject(err)
        } else {
          resolve(res)
        }
      })
    })
  },
  /**
   * SHA 256 Hash Function wrapper
   * @param {string} rawValue
   * @return {string} Hash result string in hex format
   */
  hash256: function (rawValue) {
    const hashFunc = crypto.createHash('sha256')
    hashFunc.update(rawValue)
    return hashFunc.digest('hex')
  }
}
