const nodemailer = require("nodemailer");
const ejs = require('ejs')
const {
  configs
} = require('../configs')

async function sendMail({ orderInfo, note, total }) {
  const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: configs.mailService.authUser,
      pass: configs.mailService.authPassword,
    },
  });

  const html = ejs.render(require('../templates/emailTemplate'), { orderInfo, note, total })

  const info = await transporter.sendMail({
    from: '"Yến Vua" <yenvua@gmail.com>', // sender address
    to: configs.mailService.targetEmail, // list of receivers
    subject: 'Bạn vừa có một đơn đặt hàng mới!!!', // Subject line
    text: '', // plain text body
    html, // html body
  });
}

module.exports = {
  sendMail
}
