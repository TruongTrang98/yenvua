const path = require('path');
const cors = require('cors');
const helmet = require('helmet');
const cookieParser = require('cookie-parser');
const express = require('express');
const app = express();

// CONFIG
const { configs, constants, responses, upload } = require('./configs');

// CORE
const { start: startDB } = require('./core/Database');
// const cacheIns = require('./core/Cache');

// UTILS
const {
  hashUtils,
  generateSlug,
  mailUtils,
  removeAccents,
  isVietnamesePhoneNumber,
  generateOTP,
} = require('./utils');

// (async () => {
//   console.log(await hashUtils.hash('260980'));
// })();

// MODELS
const {
  AccountModel,
  AddressModel,
  OrderModel,
  ProductModel,
  CategoryModel,
  UserAccountModel,
} = require('./models');

// MIDDLE WARES
const { jwtMiddleware, authenticate } = require('./middlewares');

const corsOptions = {
  origin: [],
  credentials: true,
};

if (configs.runtime.env === 'dev' || configs.runtime.env === 'local') {
  corsOptions.origin.push(/.*localhost.*/);
  corsOptions.origin.push(/.*127.0.0.1.*/);
}
if (configs.runtime.env === 'production') {
  corsOptions.origin.push(/.*localhost.*/);
  corsOptions.origin.push(/.*127.0.0.1.*/);
  corsOptions.origin.push(/.*api.yenvua.com.vn.*/);
}

app.use(helmet());

app.use(cors());
app.use(cookieParser());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const staticPath = path.join(__dirname, configs.express.staticFolder);
if (configs.express.staticFolder) {
  const staticConfig = { maxAge: '1m' }; // ms format ref: https://www.npmjs.com/package/ms
  app.use(
    '/',
    (req, res, next) => {
      res.header('cross-origin-resource-policy', 'cross-origin');
      next();
    },
    express.static(staticPath, staticConfig)
  );
}

const server = app.listen(configs.express.port, async (err) => {
  if (err) {
    console.error(err);
    process.exit(1);
  }

  await startDB();
  // await cacheIns.start();

  global.general = {
    responses,
    configs,
    constants,
    upload,
    hashUtils,
    jwtMiddleware,
    authenticate,
    generateSlug,
    mailUtils,
    removeAccents,
    generateOTP,
    isVietnamesePhoneNumber,
    // cache: {
    //   cacheIns,
    // },
    models: {
      AccountModel,
      AddressModel,
      ProductModel,
      OrderModel,
      CategoryModel,
      UserAccountModel,
    },
  };

  // Apply route
  require('./routes')(app);

  // error handling
  app.use((err, req, res, next) => {
    console.log(err);
    if (err === 'NotFound') {
      return res.redirect('/');
    }
    if (err === 'UnauthorizedError' || err.code === 'permission_denied') {
      return responses.unauthorizedError(res, 'txtUnauthorized');
    } else if (err === 'DBConnectError') {
      return responses.internalError(res, new Error('txtCouldnotConnectDB'));
    }
    next();
  });

  process.on('unhandledRejection', function (error) {
    console.error(error);
  });

  process.on('uncaughtException', function (error) {
    console.error(error);
  });

  //#region GEN DB ADDRESS
  // (async () => {
  //   const AddressModel = require('./models/AddressModel');
  //   let addresses = require('fs').readFileSync(
  //     path.join(__dirname, './vietnam_address.json'),
  //     {
  //       encoding: 'utf8',
  //     }
  //   );
  //   addresses = JSON.parse(addresses);

  //   let index = 1;
  //   for (const item of addresses) {
  //     const { name, codename, division_type, districts } = item;
  //     const data = {
  //       code: index,
  //       name,
  //       parentCode: 0,
  //       codename,
  //       division_type,
  //     };
  //     await AddressModel.create(data);
  //     if (districts && districts.length > 0) {
  //       const parentCode = index;
  //       index++;
  //       for (const _item of districts) {
  //         const { name, codename, division_type, short_codename } = _item;

  //         const _data = {
  //           code: index,
  //           name,
  //           parentCode,
  //           codename,
  //           short_codename,
  //           division_type,
  //         };
  //         await AddressModel.create(_data);
  //         if (_item.wards && _item.wards.length > 0) {
  //           const _parentCode = index;
  //           index++;
  //           for (const __item of _item.wards) {
  //             const { name, codename, division_type, short_codename } = __item;

  //             const __data = {
  //               code: index,
  //               name,
  //               parentCode: _parentCode,
  //               codename,
  //               short_codename,
  //               division_type,
  //             };
  //             await AddressModel.create(__data);
  //             index++;
  //           }
  //         }
  //         index++;
  //       }
  //     }
  //     index++;
  //   }
  // })();
  //#endregion

  console.log(`Server is running at port ${configs.express.port}`);
});

module.exports = server;
