const _ = require('lodash')

function logError(res, { code, detail }) {
  console.error({
    refId: res.refId || '-',
    code,
    detail: detail || 'unknow',
  })
}

module.exports = {
  success: (res, data = {}) => {
    res.status(200)
    return res.json({
      status: 'success',
      data,
    })
  },

  invalidArgumentError: (res, message) => {
    return res.status(400).json({
      status: 'error',
      error: {
        message,
      },
    })
  },

  joiValidationError: (res, error, message) => {
    logError(res, {
      code: 400,
      detail: _.get(error, 'details[0].message', 'Unknown error'),
    })
    return res.status(400).json({
      status: 'error',
      error: {
        message: message || 'txtInvalidParams',
        detail: _.get(error, 'details[0].message', 'Unknown error'),
      },
    })
  },

  unauthorizedError: (res, message) => {
    return res.status(401).json({
      status: 'error',
      error: {
        message: message || 'txtUnauthorized',
      },
    })
  },

  forbiddenError: (res, message) => {
    return res.status(403).json({
      status: 'error',
      error: {
        message,
      },
    })
  },

  notfoundError: (res, message) => {
    logError(res, { code: 404, detail: message })
    return res.status(404).json({
      status: 'error',
      error: {
        message,
      },
    })
  },

  unimplementedError: (res) => {
    return res.status(500).json({
      status: 'error',
      error: {
        message: 'txtFeatureNotImplement',
      },
    })
  },

  internalError: (res, error) => {
    return res.status(500).json({
      status: 'error',
      error: {
        message: error.message || error || 'txtInternalServerError',
      },
    })
  },
  sessionExpire: (res, message) => {
    return res.status(440).json({
      status: 'error',
      error: {
        message: message || 'txtSessionExpire',
      },
    })
  },
  exceedLimit: (res, message) => {
    return res.status(429).json({
      status: 'error',
      error: {
        message: message || 'txtExceedLimit',
      },
    })
  },
  lockedError: (res, message) => {
    return res.status(423).json({
      status: 'error',
      error: {
        message: message || 'txtLocked',
      },
    })
  },
}
