const multer = require('multer');
const path = require('path');
const fs = require('fs');

const staticPath = path.join(
  __dirname,
  `../${process.env.EXPRESS_STATIC_FOLDER}`
);
if (process.env.EXPRESS_STATIC_FOLDER) {
  if (!fs.existsSync(staticPath)) {
    fs.mkdirSync(staticPath);
  }
}

if (!fs.existsSync(path.join(__dirname, '../public/images'))) {
  fs.mkdirSync(path.join(__dirname, '../public/images'));
}

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, '../public/images'));
  },
  filename: function (req, file, cb) {
    let extFile = file.originalname.split('.');
    extFile = extFile[extFile.length - 1];
    file.ext = extFile;
    cb(null, `${Date.now()}.${extFile}`);
  },
});

const upload = multer({ storage: storage, limits: { fileSize: '50mb' } });

module.exports = upload;
