const globalConfigs = {
  runtime: {
    env: process.env.NODE_ENV || 'dev',
  },
  express: {
    port: process.env.PORT || 3000,
    hostName: process.env.HOSTNAME,
    staticFolder: process.env.EXPRESS_STATIC_FOLDER,
  },
  mongo: {
    uri: process.env.MONGODB_URI || 'mongodb://localhost:27017/yenvua',
  },
  redis: {
    uri: process.env.REDIS_URI || 'redis://localhost:6379',
    otpTTL: process.env.OTP_TTL_MS || 2 * 60,
  },
  token: {
    secretKey: process.env.JWT_SECRET_KEY || 'ntT@123',
    ADMIN: {
      expiresIn: process.env.JWT_ADMIN_EXPIRES_IN || '30m',
    },
    USER: {
      expiresIn: process.env.JWT_USER_EXPIRES_IN || '15m',
    },
  },
  mailService: {
    isSendMail: process.env.FEAT_SEND_MAIL === '1' || false,
    authUser: process.env.MAIL_AUTH_USER,
    authPassword: process.env.MAIL_AUTH_PASSWORD,
    targetEmail: (process.env.A_QUANG_EMAIL || '').split(','),
  },
};

Object.freeze(globalConfigs);

module.exports = globalConfigs;
