const ORDER_STATUS = {
  CREATED: 'CREATED',
  DONE: 'DONE',
  CANCELED: 'CANCELED',
};

const ACCOUNT_TYPE = {
  USER: 'USER',
  ADMIN: 'ADMIN',
};

module.exports = {
  ORDER_STATUS,
  ACCOUNT_TYPE,
};
