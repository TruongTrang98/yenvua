(async () => {
  const AddressModel = require('../../models/AddressModel');
  let addresses = require('fs').readFileSync('./vietnam_address.json', {
    encoding: 'utf8',
  });
  addresses = JSON.parse(addresses);

  let index = 1;
  for (const item of addresses) {
    const { name, codename, division_type, districts } = item;
    const data = {
      code: index,
      name,
      parentCode: 0,
      codename,
      division_type,
    };
    await AddressModel.create(data);
    if (districts && districts.length > 0) {
      const parentCode = index;
      index++;
      for (const _item of districts) {
        const { name, codename, division_type, short_codename } = _item;

        const _data = {
          code: index,
          name,
          parentCode,
          codename,
          short_codename,
          division_type,
        };
        await AddressModel.create(_data);
        if (_item.wards && _item.wards.length > 0) {
          const _parentCode = index;
          index++;
          for (const __item of _item.wards) {
            const { name, codename, division_type, short_codename } = __item;

            const __data = {
              code: index,
              name,
              parentCode: _parentCode,
              codename,
              short_codename,
              division_type,
            };
            await AddressModel.create(__data);
          }
        }
      }
    }
    index++;
  }
})();
